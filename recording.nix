{ }:

let
  pkgs = import <nixpkgs> { };
in
  pkgs.stdenv.mkDerivation {
    name = "peek_screen_recorder";
    src = ./.;
    buildInputs = [
      pkgs.ffmpeg
      pkgs.glib
      pkgs.gst_all_1.gst-plugins-good
      pkgs.gst_all_1.gst-plugins-ugly
      pkgs.keybinder
      pkgs.peek
    ];
  }
