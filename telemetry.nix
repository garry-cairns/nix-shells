{ }:

let
  pkgs = import <nixpkgs> { };
in
  pkgs.stdenv.mkDerivation {
    name = "baseLibrary";
    src = ./.;
    buildInputs = [
      pkgs.libcap
      pkgs.gcc
      pkgs.go
      pkgs.golint
      pkgs.otel-cli
    ];
    shellHook = ''
      alias runbuilder="/home/garry/code/nixpkgs/result/bin/ocb --config /home/garry/code/work/opentelemetry-collector-contrib/builder-config.yaml";
      alias runcollector="/home/garry/code/work/opentelemetry-collector-contrib/otelcol-dev/otelcol-dev --config=/home/garry/code/work/opentelemetry-collector-contrib/otelcol-dev/config.yaml";
      alias sendspan="otel-cli exec --service my-service --name \"curl google\" --endpoint localhost:4317 curl https://google.com";
    '';
  }
