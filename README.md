# nix-shells

These are the `default.nix` files I use on my system. I keep them in a `nix-shells` directory under source control, which is what you see here, and them symlink to them. For example, to set up my `~/data` directory with my `data.nix` derivation, I use the command

    ln -s ~/nix-shells/data.nix ~/data/default.nix
