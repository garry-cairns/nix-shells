{ }:

let
  pkgs = import <nixpkgs> { };
in
  pkgs.stdenv.mkDerivation {
    name = "basePublishing";
    src = ./.;
    buildInputs = [
      pkgs.calibre
      pkgs.libreoffice
      pkgs.pandoc
    ];
    shellHook = ''
      alias cook="pandoc --css personalRecipes/personalRecipes.css -o personalRecipes/personalRecipes.epub personalRecipes/personalRecipes.org";
    '';
  }
