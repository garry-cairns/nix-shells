{ }:

let
  pkgs = import <nixpkgs> { };
in
  pkgs.stdenv.mkDerivation {
    name = "basePhotography";
    src = ./.;
    buildInputs = [
      pkgs.darktable
      pkgs.gimp
      pkgs.lcms
      pkgs.scribus
    ];
  }
