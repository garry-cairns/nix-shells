{ }:

let
  pkgs = import <nixpkgs> { };
  RWithPackages = pkgs.rWrapper.override{ packages = with pkgs.rPackages; [ arm bayesplot gapminder gganimate ggplot2 knitr magick markovchain matlib plotly rstanarm rstan survey ]; };
in
  pkgs.stdenv.mkDerivation {
    name = "baseDataAnalysis";
    src = ./.;
    buildInputs = [
      pkgs.gifski
      pkgs.libreoffice
      RWithPackages
    ];
  }
