{ }:

let
  pkgs = import <nixpkgs> { };
in
  pkgs.stdenv.mkDerivation {
    name = "baseLibrary";
    src = ./.;
    buildInputs = [
      pkgs.calibre
      pkgs.gmp
      pkgs.python38Full
      pkgs.python38Packages.pycryptodome
    ];
  }
